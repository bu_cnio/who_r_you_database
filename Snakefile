import glob
import pandas as pd
import os.path

from snakemake.utils import min_version


#### GLOBAL PARAMETERS AND CONFIG ####

min_version('5.4')
configfile: "config.yaml"
INDIR 					=config['inputdir'] 
OUTDIR					=config['outputdir']
PROJECTS				= pd.read_csv(config['projects'], sep='\t')['Projects']
INDATA					= ['mRNA','DNA']
PROJECTS_NORM			= pd.read_csv(config['projects_norm'], sep='\t')['Projects']
#### GLOBAL SCOPE FUNCTIONS ####

def get_resource(rule,resource):
	'''
	Attempt to parse config.yaml to retrieve resources available for each rule.
	It will revert to default if a key error is found
	'''
	try:
		return config['rules'][rule]['res'][resource]
	except KeyError:
		print(f'Failed to resolve resource config for rule {rule}/{resource}: default parameters have been set')
		return config['rules']['default']['res'][resource]

def get_mrna_file(wildcards):
	project_dir = f'{INDIR}/transcriptome_profiling/gene_expression_quantification/HTSeq-counts/XENA/' + wildcards.project + '/TCGA-'+ wildcards.project +'.htseq_counts.tsv'
	return glob.glob(project_dir)

def get_dna_file(wildcards):
	project_dir = f'{INDIR}/simple_nucleotide_variation/MuTect2/' + wildcards.project + '/*.maf'
	return glob.glob(project_dir)

def get_clinical_file(wildcards):
	project_dir = f'{INDIR}/clinical_data/TCGA-' + wildcards.project + '.GDC_phenotype.tsv'
	return glob.glob(project_dir)

def get_drug_file(wildcards):
	project_dir = f'{INDIR}/thearpies/Projects/nationwidechildrens.org_clinical_drug_' + wildcards.project + '.txt'
	return glob.glob(project_dir)

def get_FPKM_file(wildcards):
	project_dir = f'{INDIR}/transcriptome_profiling/gene_expression_quantification/FPKM/TCGA-' + wildcards.project + '.htseq_fpkm.tsv'
	return glob.glob(project_dir)

def get_TMM_file(wildcards):
	project_dir = f'{OUTDIR}/transcriptome_profiling/gene_expression_quantification/FPKM/TCGA-' + wildcards.project + '.htseq_fpkm.tsv'
	return glob.glob(project_dir)




def get_subcluster(wildcards):
	checkpoint_output = checkpoints.Subcluster_input.get(**wildcards).output[0]
	return	expand(OUTDIR + '/clustering/Cluster_{cluster}_results/Cluster_tables/Subcluster_table_clinical.csv',
			cluster = glob_wildcards(os.path.join(checkpoint_output, "Cluster_{cluster}_results/")).cluster)

			
#### RULES ####

rule all:
	input:
		OUTDIR + '/mRNA/TCGA_pancancer_RNAseq_TMM_common_genes.csv',
		OUTDIR + '/mutations/aproach2_cgi/mutations_pancancer_preprocess.csv',
		OUTDIR + '/clinical/clinical_pancancer_matrix.tsv',
		OUTDIR + '/clinical/survival_pancancer_matrix.tsv',
		OUTDIR + '/clinical/Drugs_combination_pancancer_matrix.tsv',
		OUTDIR + '/clinical/Drugs_individual_pancancer_matrix_preprocess.tsv',
		OUTDIR + '/pathways/TCGA_pancancer_singscore.csv',
		OUTDIR + '/immune/pancancer_immune_matrix.txt',
		OUTDIR + '/immune/pancancer_immune_population.txt',
		OUTDIR + '/clinical/clinical_pancancer_matrix_clustering.tsv',
		OUTDIR + '/clinical/Complete_table_patients.txt',
		OUTDIR + '/Cluster_tables/Cluster_table_clinical.csv',
		OUTDIR + '/Cluster_tables/Cluster_table_drugs.csv',
		OUTDIR + '/Cluster_tables/Cluster_table_mutations.csv',
		OUTDIR + '/Cluster_tables/Cluster_table_immune_populations.csv',
		OUTDIR + '/Cluster_tables/Cluster_table_pathways.csv',
		OUTDIR + '/Cluster_tables/CLUSTERS.txt',
		OUTDIR + 'mutation_features.gmt',
		OUTDIR + 'drugs_features.gmt',
		OUTDIR + 'clinical_features.gmt',
		OUTDIR + 'all_features.gmt',
		get_subcluster,
rule get_filter_matrix:
	input:
		get_mrna_file
	output:
		OUTDIR + '/mRNA/comon_genes/{project}/{project}_genes.csv',
		OUTDIR + '/mRNA/filtered/{project}/{project}_filtered.csv',
		OUTDIR + '/mRNA/filtered/{project}/{project}_all_genes_TMM.csv'

	threads:
		get_resource('get_filter_matrix', 'threads')
	resources:
		mem_mb=get_resource('get_filter_matrix','mem_mb')
	conda:
		'envs/filter_mrna.yaml'
	script:
		'scripts/Filter_genes_mRNA.R'



rule get_common_genes:
	input:
		expand(OUTDIR + '/mRNA/comon_genes/{project}/{project}_genes.csv', project=PROJECTS),
	output:
		OUTDIR + '/mRNA/comon_genes/common_genes.csv'
	threads:
		get_resource('get_common_genes', 'threads')
	resources:
		mem_mb=get_resource('get_common_genes', 'mem_mb')
	run:
		shell("awk 'NR == 1 || FNR > 1' {input} > {output}")
		all_genes = pd.read_csv(output[0], sep='\t')
		gene_count = all_genes['Gene'].value_counts()
		gene_count = gene_count.reset_index()
		gene_count.columns = ['Gene', 'Count']
		gene_count.to_csv(output[0],sep= '\t', index = False)


rule get__mrna_pancancer_matrix:
	input:
		expand(OUTDIR + '/mRNA/filtered/{project}/{project}_filtered.csv', project = PROJECTS)

		
	output:
		OUTDIR + '/mRNA/TCGA_pancancer_RNAseq.csv'
	threads:
		get_resource('get__mrna_pancancer_matrix', 'threads')
	resources:
		mem_mb=get_resource('get__mrna_pancancer_matrix', 'mem_mb')
	shell:	
		"""
		source scripts/get_common_genes.sh
		xjoin {input} > temp_m.txt
		grep Gene temp_m.txt > header_m.txt
		grep -v Gene temp_m.txt > body_m.txt
		cat header_m.txt body_m.txt > {output}
		rm header_m.txt
		rm body_m.txt
		rm temp_m.txt
		"""



rule TMM_pancancer:
	input:
		OUTDIR + '/mRNA/TCGA_pancancer_RNAseq.csv',
	
	output:
		OUTDIR + '/mRNA/TCGA_pancancer_RNAseq_TMM.csv',

	threads:
		get_resource('get__TMM_pancancer_matrix', 'threads')
	resources:
		mem_mb=get_resource('get__TMM_pancancer_matrix', 'mem_mb')
	conda:
		'envs/filter_mrna.yaml'
	script:
		'scripts/TMM.R'




rule filter_by_common_genes:
	input:
		OUTDIR + '/mRNA/comon_genes/common_genes.csv',
		OUTDIR + '/mRNA/TCGA_pancancer_RNAseq_TMM.csv'

	output:
		OUTDIR + '/mRNA/TCGA_pancancer_RNAseq_TMM_common_genes.csv',

	threads:
		get_resource('filter_by_common_genes', 'threads')
	resources:
		mem_mb=get_resource('filter_by_common_genes', 'mem_mb')
	run:	
		
		genes = pd.read_csv(input[0], sep = '\t', index_col = 0)
		genes = genes[genes.Count == max(genes.Count)]
		matrix = pd.read_csv(input[1], sep = '\t', index_col = 0)
		matrix = matrix[matrix.index.isin(genes.index)]
		matrix.to_csv(output[0], sep = '\t', index = True)


rule mutation_filtering_2_pancancer:
	input:
		get_dna_file,
		INDIR + '/simple_nucleotide_variation/catalog_of_validated_oncogenic_mutations_latest/catalog_of_validated_oncogenic_mutations.tsv'
	output:
		OUTDIR + '/mutations/aproach2_cgi/Projects/{project}/{project}_mutation_matrix.csv'
	threads:
		get_resource('get_pancancer_mutation_matrix', 'threads')
	resources:
		mem_mb=get_resource('get_pancancer_mutation_matrix', 'mem_mb')
	script:	
		'scripts/filter_maf_aproach2_pancancer.py'


rule get_pancancer_mutation_matrix_2:
	input:
		expand(OUTDIR + '/mutations/aproach2_cgi/Projects/{project}/{project}_mutation_matrix.csv', project = PROJECTS)

	output:
		OUTDIR + '/mutations/aproach2_cgi/mutations_pancancer.csv'
	threads:
		get_resource('get_pancancer_mutation_matrix', 'threads')
	resources:
		mem_mb=get_resource('get_pancancer_mutation_matrix', 'mem_mb')
	shell:	
		'''
		awk -f scripts/mutation_pancancer_matrix.awk {input} > {output}
		'''


rule get_pancancer_mutation_matrix_filtered_2:
	input:
		OUTDIR + '/mutations/aproach2_cgi/mutations_pancancer.csv'
	output:
		OUTDIR + '/mutations/aproach2_cgi/mutations_pancancer_preprocess.csv',
	script:	
		'scripts/Preprocess_mutations.py'




rule get_clinical_features:
	input:
		get_clinical_file

	output:
		OUTDIR + '/clinical/Projects/{project}/{project}_clinical_matrix.tsv'
	threads:
		get_resource('get_clinical_features', 'threads')
	resources:
		mem_mb=get_resource('get_clinical_features', 'mem_mb')
	script:	
		'scripts/filter_clinical_matrix.py'

rule get_pancancer_clinical_matrix:
	input:
		expand(OUTDIR + '/clinical/Projects/{project}/{project}_clinical_matrix.tsv', project = PROJECTS)
	output:
		OUTDIR + '/clinical/clinical_pancancer_matrix.tsv'
	threads:
		get_resource('get_pancancer_clinical_matrix', 'threads')
	resources:
		mem_mb=get_resource('get_pancancer_clinical_matrix', 'mem_mb')
	shell:	
		'''
		 awk 'NR == 1 || FNR > 1' {input} > {output}
		'''
rule get_survival_features:
	input:
		INDIR + '/pancancer_survival/curated_tcga_survival.csv'
	output:
		OUTDIR + '/clinical/survival_pancancer_matrix.tsv'
	threads:
		get_resource('get_survival_features', 'threads')
	resources:
		mem_mb=get_resource('get_survival_features', 'mem_mb')
	script:	
		'scripts/filter_survival.py'

rule get_drug_features:
	input:
		get_drug_file,
		INDIR + '/thearpies/DrugCorrection1.csv'
	output:
		OUTDIR + '/clinical/Project_drugs/{project}_drug_individual_matrix.tsv',
		OUTDIR + '/clinical/Project_drugs/{project}_drug_combination_matrix.tsv'

	threads:
		get_resource('get_survival_features', 'threads')
	resources:
		mem_mb=get_resource('get_survival_features', 'mem_mb')
	script:	
		'scripts/filter_drugs.py'

rule get_pancancer_indivudal_drug:
	input:
		expand(OUTDIR + '/clinical/Project_drugs/{project}_drug_individual_matrix.tsv', project = PROJECTS)

	output:
		OUTDIR + '/clinical/Drugs_individual_pancancer_matrix.tsv'
	threads:
		get_resource('get_pancancer_indivudal_drug', 'threads')
	resources:
		mem_mb=get_resource('get_pancancer_indivudal_drug', 'mem_mb')
	shell:	
		'''
		awk -f scripts/drug_pancancer_matrix.awk {input} > {output}
		'''
rule get_pancancer_combination_drug:
	input:
		expand(OUTDIR + '/clinical/Project_drugs/{project}_drug_combination_matrix.tsv', project = PROJECTS)

	output:
		OUTDIR + '/clinical/Drugs_combination_pancancer_matrix.tsv'
	threads:
		get_resource('get_pancancer_combination_drug', 'threads')
	resources:
		mem_mb=get_resource('get_pancancer_combination_drug', 'mem_mb')
	shell:	
		'''
		awk -f scripts/drug_pancancer_matrix.awk {input} > {output}
		'''
rule get_singscore:
	input:
		OUTDIR + '/mRNA/filtered/{project}/{project}_all_genes_TMM.csv',
		'databases/h.all.v7.0.symbols.gmt'
	output:
		OUTDIR + '/pathways/{project}/{project}_TCGA_singscore.csv'
		
	threads:
		get_resource('get_singscore', 'threads')
	resources:
		mem_mb=get_resource('get_singscore','mem_mb')
	conda:
		'envs/singscore.yml'
	script:
		'scripts/Singscore.R'


rule get_mrna_Singscore_matrix:
	input:
		expand(OUTDIR + '/pathways/{project}/{project}_TCGA_singscore.csv', project = PROJECTS),
	
	output:
		OUTDIR + '/pathways/TCGA_pancancer_singscore.csv'
	threads:
		get_resource('get__mrna_pancancer_matrix', 'threads')
	resources:
		mem_mb=get_resource('get__mrna_pancancer_matrix', 'mem_mb')
	shell:	
		"""
		source scripts/get_common_genes.sh
		xjoin {input} > temp_m.txt
		grep Pathways temp_m.txt > header_m.txt
		grep -v Pathways temp_m.txt > body_m.txt
		cat header_m.txt body_m.txt > {output}
		rm header_m.txt
		rm body_m.txt
		rm temp_m.txt
		"""


rule get_immune_matrix:
	input:
		INDIR + '/immune_subtype/Immune_subtypes.csv',
		INDIR + '/msi/msi_TCGA.csv'
	output:
		OUTDIR + '/immune/pancancer_immune_matrix.txt',
		OUTDIR + '/immune/pancancer_immune_population.txt',
	threads:
		get_resource('get_immune_matrix', 'threads')
	resources:
		mem_mb=get_resource('get_immune_matrix','mem_mb')
	script:
		'scripts/immune_matrix.py'

rule clustering_TCGA:
	input:
		OUTDIR + '/mRNA/TCGA_pancancer_RNAseq_TMM_common_genes.csv',
		OUTDIR + '/clinical/clinical_pancancer_matrix.tsv',

	output:
		OUTDIR + '/clinical/clinical_pancancer_matrix_clustering.tsv',

	threads:
		get_resource('clustering', 'threads')
	resources:
		mem_mb=get_resource('clustering','mem_mb'),
		walltime= get_resource('clustering','walltime')
	conda:
		'envs/M3C.yml'
	script:
		'scripts/clustering_M3C.R'

		

rule Completa_table:
	input:
		OUTDIR + '/clinical/clinical_pancancer_matrix_clustering.tsv',
		OUTDIR + '/clinical/Drugs_combination_pancancer_matrix.tsv',
		OUTDIR + '/clinical/survival_pancancer_matrix.tsv',
		OUTDIR + '/immune/pancancer_immune_matrix.txt',
		OUTDIR + '/mutations/aproach2_cgi/mutations_pancancer_preprocess.csv',
	output:
		OUTDIR + '/clinical/Complete_table_patients.txt',

	threads:
		get_resource('clustering', 'threads')
	script:
		'scripts/Complete_patient_table.R'

rule Preprocess_Drugs:
	input:
		OUTDIR + '/clinical/Drugs_individual_pancancer_matrix.tsv',

	output:
		OUTDIR + '/clinical/Drugs_individual_pancancer_matrix_preprocess.tsv',

	conda:
		'envs/filter_mrna.yaml'
	script:
		'scripts/Preprocess_drugs.R'

rule Preprocess_continous:
	input:
		OUTDIR + '/pathways/TCGA_pancancer_singscore.csv',
		OUTDIR + '/immune/pancancer_immune_population.txt',
		
	output:
		OUTDIR + '/pathways/TCGA_pancancer_singscore_deciles.csv',
		OUTDIR + '/immune/pancancer_immune_population_deciles.txt',
	conda:
		'envs/gmt.yml'
	script:
		'scripts/Preprocess_Continous.R'



rule Cluster_tables:
	input:
		OUTDIR + '/clinical/clinical_pancancer_matrix_clustering.tsv',
		OUTDIR + '/immune/pancancer_immune_matrix.txt',
		OUTDIR + '/clinical/Drugs_individual_pancancer_matrix_preprocess.tsv',
		OUTDIR + '/mutations/aproach2_cgi/mutations_pancancer_preprocess.csv',
		OUTDIR + '/immune/pancancer_immune_population.txt',
		OUTDIR + '/pathways/TCGA_pancancer_singscore.csv',
		OUTDIR + '/clinical/survival_pancancer_matrix.tsv',
	output:
		OUTDIR + '/Cluster_tables/Cluster_table_clinical.csv',
		OUTDIR + '/Cluster_tables/Cluster_table_drugs.csv',
		OUTDIR + '/Cluster_tables/Cluster_table_mutations.csv',
		OUTDIR + '/Cluster_tables/Cluster_table_immune_populations.csv',
		OUTDIR + '/Cluster_tables/Cluster_table_pathways.csv',
		OUTDIR + '/Cluster_tables/CLUSTERS.txt',


	script:
		'scripts/Cluster_table.R'

checkpoint Subcluster_input:
	input:
		OUTDIR + '/clinical/clinical_pancancer_matrix_clustering.tsv',
		OUTDIR + '/immune/pancancer_immune_matrix.txt',
		OUTDIR + '/clinical/Drugs_individual_pancancer_matrix_preprocess.tsv',
		OUTDIR + '/mutations/aproach2_cgi/mutations_pancancer_preprocess.csv',
		OUTDIR + '/immune/pancancer_immune_population.txt',
		OUTDIR + '/pathways/TCGA_pancancer_singscore.csv',
		OUTDIR + '/clinical/survival_pancancer_matrix.tsv',
	output:
		directory(OUTDIR + '/clustering/'),
	script:
		'scripts/subcluster_input_table.R'
	
checkpoint Subcluster_tables:
	input:
		OUTDIR + '/clustering/Cluster_{cluster}_results/input_tables/clinical_pancancer_matrix_clustering.tsv',
		OUTDIR + '/clustering/Cluster_{cluster}_results/input_tables/pancancer_immune_matrix.txt',
		OUTDIR + '/clustering/Cluster_{cluster}_results/input_tables/Drugs_individual_pancancer_matrix_preprocess.tsv',
		OUTDIR + '/clustering/Cluster_{cluster}_results/input_tables/mutations_pancancer_preprocess.csv',
		OUTDIR + '/clustering/Cluster_{cluster}_results/input_tables/pancancer_immune_population.txt',
		OUTDIR + '/clustering/Cluster_{cluster}_results/input_tables/TCGA_pancancer_singscore.csv',
		OUTDIR + '/clustering/Cluster_{cluster}_results/input_tables/survival_pancancer_matrix.tsv',
	output:
		OUTDIR + '/clustering/Cluster_{cluster}_results/Cluster_tables/Subcluster_table_clinical.csv',
		OUTDIR + '/clustering/Cluster_{cluster}_results/Cluster_tables/Subcluster_table_drugs.csv',
		OUTDIR + '/clustering/Cluster_{cluster}_results/Cluster_tables/Subcluster_table_mutations.csv',
		OUTDIR + '/clustering/Cluster_{cluster}_results/Cluster_tables/Subcluster_table_immune_populations.csv',
		OUTDIR + '/clustering/Cluster_{cluster}_results/Cluster_tables/Subcluster_table_pathways.csv',
		OUTDIR + '/clustering/Cluster_{cluster}_results/Cluster_tables/Subcluster_CLUSTERS.csv',
	script:
		'scripts/Subcluster_table.R'

rule create_gmt:
	input:
		OUTDIR + '/clinical/Complete_table_patients.txt',
		OUTDIR + '/clinical/Drugs_individual_pancancer_matrix_preprocess.tsv',
		OUTDIR + '/mutations/aproach2_cgi/mutations_pancancer_preprocess.csv',
		OUTDIR + '/pathways/TCGA_pancancer_singscore_deciles.csv',
		OUTDIR + '/immune/pancancer_immune_population_deciles.txt',
	output:
		OUTDIR + 'mutation_features.gmt',
		OUTDIR + 'drugs_features.gmt',
		OUTDIR + 'clinical_features.gmt',
		OUTDIR + 'all_features.gmt'
	conda:
		'envs/gmt.yml'
	script:
		'scripts/create_gmt.R'

rule gmt_statistics:
	input:
		mut_gmt  =  OUTDIR + 'mutation_features.gmt',
		drug_gmt = OUTDIR + 'drugs_features.gmt',
		cli_gmt  =  OUTDIR + 'clinical_features.gmt',
		exp = OUTDIR + '/mRNA/TCGA_pancancer_RNAseq_TMM_common_genes.csv',

	output:
		mut_st = OUTDIR + 'gmt_mut_statistics.txt',
		drug_st = OUTDIR + 'gmt_drug_statistics.txt',
		cli_st = OUTDIR + 'gmt_cli_statistics.txt',

	conda:
		'envs/gmt.yml'
	script:
		'scripts/singscore.R'
