import pandas as pd
import re
import numpy as np
# we open the clinical file
cli = pd.read_csv(snakemake.input[0], sep = '\t' , index_col = 0)
cli = cli[['tumor_stage.diagnoses', 'disease_code',
'vital_status.demographic', 'person_neoplasm_cancer_status']]


cli = cli.filter(regex = '-01[A-Z]$' , axis = 0)


cli['ID_samples'] = cli.index.values

cli = cli.replace( regex = '-01[A-Z]$', value = '')
cli = cli.drop_duplicates(subset = ['ID_samples'])
cli.index = cli.ID_samples
cli = cli.drop('ID_samples' , axis = 1)

#we collapse stage into I II II and IV



# we change names 

cli = cli.replace(regex = ['not reported','is','i/ii', 'nos', 'Not Reported'] , value = np.nan)

cli['vital.status' == 'Not Reported'] = ''
cli['person_neoplasm_cancer_status' == 'Not Reported'] = ''
cli['person_neoplasm_cancer_status' == 'Not Reported'] = ''



cli = cli.replace(regex =['stage ','[a,b,c,x]'] , value = '')
cli['tumor_stage.diagnoses'] = cli['tumor_stage.diagnoses'].apply(lambda x : str(x).upper() )
cli['tumor_stage.diagnoses'] = cli['tumor_stage.diagnoses'].apply(lambda x : x.upper() )

print(cli.groupby('tumor_stage.diagnoses').sum())


cli = cli.rename(columns = {'tumor_stage.diagnoses' : 'Stage' , 'vital_status.demographic' : 'Vital status' , 'person_neoplasm_cancer_status' : 'Tumor Status'})
cli = cli.rename(index = lambda x : x.replace('-','.'))
cli = cli[['Stage',	'disease_code' ,'Vital status',	'Tumor Status']]

cli.to_csv(snakemake.output[0] , sep = '\t')
