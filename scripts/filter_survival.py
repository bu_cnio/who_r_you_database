import pandas as pd
# we open the clinical file
cli = pd.read_csv(snakemake.input[0], sep = ',', index_col = 0)
cli = cli[['new_tumor_event_type','DSS','DSS.time','PFI','PFI.time','OS','OS.time']]


cli = cli.replace(regex =['.*','Metastasis','.*'] , value = 'Distant Metastasis')

cli = cli.replace(['Locoregional Recurrence', 'Recurrence'
,'Locoregional (Urothelial tumor event)','Locoregional Disease','Extrahepatic Recurrence','Intrahepatic Recurrence' , 'Progression of Disease'
, "Intrapleural Progression","Biochemical evidence of disease","Regional lymph node","Locoregional Recurrence|Regional lymph node"],'Recurrence',)


cli = cli.replace("Metastatic",'Distant Metastasis')

cli = cli.replace(['[Not Available]',"[Not Available]|[Not Available]"],'NA')

cli = cli.replace([""],'Not Recurrence')


cli = cli.replace(['New Primary Tumor',"New primary melanoma"],'New Primary Tumor')

times = ['DSS.time','PFI.time','OS.time']

for t in times:
    cli[t] = cli[t] / (365/12)


cli = cli.rename(index = lambda x : x.replace('-','.'))

cli.to_csv(snakemake.output[0] , sep = '\t')
