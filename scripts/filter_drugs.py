
import pandas as pd

####################################################################################
# FUNCTIONS
def splitDataFrameList(df,target_column,separator):
    ''' df = dataframe to split,
    target_column = the column containing the values to split
    separator = the symbol used to perform the split
    returns: a dataframe with each entry for the target column separated, with each element moved into a new row. 
    The values in the other columns are duplicated across the newly divided rows.
    '''
    def splitListToRows(row,row_accumulator,target_column,separator):
        split_row = row[target_column].split(separator)
        for s in split_row:
            new_row = row.to_dict()
            new_row[target_column] = s
            row_accumulator.append(new_row)
    new_rows = []
    df.apply(splitListToRows,axis=1,args = (new_rows,target_column,separator))
    new_df = pd.DataFrame(new_rows)
    return new_df




# we open the drug file and the correction file
drug = pd.read_csv(snakemake.input[0], sep = '\t', index_col = 2 ,  header= 1)
drug = drug[['bcr_patient_barcode','drug_name','measure_of_response', 'form_completion_date']]
drug = drug.drop(['CDE_ID:'])
drug = drug[~ drug.measure_of_response.isin(['[Not Applicable]'
                                                , '[Unknown]'
                                                , '[Not Available]'
                                                ,'[Discrepancy]'])]


if drug.shape[0] > 1:
    

    corr = pd.read_csv(snakemake.input[1], sep = ',', index_col = 0)
    # We merge both dataframes to get corrected name

    drug = pd.merge(drug,corr, left_on = 'drug_name', right_on = 'OldName')

    drug['form_completion_date'] = pd.to_datetime(drug.form_completion_date)
    drug = drug.sort_values(by ='form_completion_date')

    # We are going to check combination of therapies
    drug = drug.drop(['drug_name', 'form_completion_date' ], axis = 1)
    drug = drug.drop_duplicates(subset = ['bcr_patient_barcode'
                                                ,'Correction'])

    # Spand combined treatments

    drug = splitDataFrameList( df = drug , target_column = 'Correction', separator = '+' )
    drug = drug.drop_duplicates(subset = ['bcr_patient_barcode'
                                                ,'Correction'])

    # Create combinations 



    drug = drug.sort_values(by = 'Correction')

    params = {

    'measure_of_response' : lambda x : ','.join(sorted(pd.Series.unique(x))),
    'Correction' : lambda x : ','.join(sorted(pd.Series.unique(x)))

    }


    comb = drug.groupby('bcr_patient_barcode').agg(params).reset_index()

    drug = drug.pivot( index = 'bcr_patient_barcode'
        , columns = 'Correction' 
        , values = 'measure_of_response')


    comb= comb.pivot( index = 'bcr_patient_barcode'
        , columns = 'Correction' 
        , values = 'measure_of_response')
    
    
    comb = comb.T
    drug = drug.T
    
    drug = drug.rename(columns = lambda x : x.replace('-','.'))
    comb = comb.rename(columns = lambda x : x.replace('-','.'))



    comb.to_csv(snakemake.output[1] , sep = '\t')
    drug.to_csv(snakemake.output[0] , sep = '\t')



else:
    
    open(snakemake.output[0], 'a').close()
    open(snakemake.output[1], 'a').close()


