BEGIN { FS = "\t" ; OFS = " \t"}

FNR==1 { ++file }

{
a[$1,file] = $2 FS $3
++seen[$1]
}

END {
for (j in seen) {
split(j, b, SUBSEP)
s = b[1] FS b[2]
for (i=1; i<=file; ++i) {
s = s FS (j SUBSEP i in a ? a[j,i] : "NA" FS "NA")
}
print s
}
}