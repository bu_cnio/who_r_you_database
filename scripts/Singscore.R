###########################################
#Filter_genes_mRNA.R
#'@author : carlos Carretero Puche
#'@email : carlos_carre11@hotmail.com
#'@Description: This script creates scores for pathway activationn in ingle patient
#' 
############################################################
###FUNCTIONS


#Multiscore
#'@rank : ranking matrix  obtained by function rankGenes
#'@gen_col : genset obtained by function getGmt
Multiscore <- function(rank,gen_col){
  n = 1
  for (genset in gen_col){
    score <- simpleScore(rank, genset)
    colnames(score) <- c(setName(genset),'-')
    if (n == 1){
      score <- simpleScore(rank,genset)
      result <- data.frame(score[,1], row.names = rownames(score))
      colnames(result) <- setName(genset)
      
    }
    
    else{
      score <- simpleScore(rank,genset)
      score <- data.frame(score[,1], row.names = rownames(score))
      colnames(score) <-  setName(genset)
      result <- cbind(result,score)
    }
    
    n = n+1
  }
  return(result)
  
}

##########################################################################
library(GSEABase)
library(singscore)


data <- read.table(snakemake@input[[1]][[1]]
                   , sep = '\t'
                   , row.names = 1
                   , header = T
                   ,check.names = F)

genset <- getGmt(snakemake@input[[2]][[1]])


rank <- rankGenes(data)

scores <- Multiscore(rank , genset)
scores <- as.data.frame(t(scores))
scores$Pathways <- rownames(scores)
scores <- scores[,c(ncol(scores),1:ncol(scores)-1)]

print(scores[1:20,1:20])

write.table( scores
            , file = snakemake@output[[1]][[1]]
            , sep = '\t'
            ,col.names = T
            , row.names = F
            , quote = F)