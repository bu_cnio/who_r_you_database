import pandas as pd 

#########################################
#1 Filter and prepare immune matrix

i_df = pd.read_csv(snakemake.input[0], sep = '\t')
i_df = i_df[['TCGA Participant Barcode','TCGA Study','Immune Subtype',
'Leukocyte Fraction','SNV Neoantigens','Indel Neoantigens' ,'B Cells Memory' , 'B Cells Naive' , 'Dendritic Cells Activated' 
, 'Dendritic Cells Resting' , 'Eosinophils' , 'Macrophages M0' , 'Macrophages M1' , 'Macrophages M2' 
, 'Mast Cells Activated' , 'Mast Cells Resting' , 'Monocytes' , 'Neutrophils' , 'NK Cells Activated' 
, 'NK Cells Resting' , 'Plasma Cells' , 'T Cells CD4 Memory Activated' , 'T Cells CD4 Memory Resting' 
, 'T Cells CD4 Naive' , 'T Cells CD8' , 'T Cells Follicular Helper' , 'T Cells gamma delta' , 
'T Cells Regulatory Tregs']]

# Obtain Absolute fraction of immune population using Leuckocyte fraction


for ip in ['B Cells Memory' , 'B Cells Naive' , 'Dendritic Cells Activated' 
, 'Dendritic Cells Resting' , 'Eosinophils' , 'Macrophages M0' , 'Macrophages M1' , 'Macrophages M2' 
, 'Mast Cells Activated' , 'Mast Cells Resting' , 'Monocytes' , 'Neutrophils' , 'NK Cells Activated' 
, 'NK Cells Resting' , 'Plasma Cells' , 'T Cells CD4 Memory Activated' , 'T Cells CD4 Memory Resting' 
, 'T Cells CD4 Naive' , 'T Cells CD8' , 'T Cells Follicular Helper' , 'T Cells gamma delta' , 
'T Cells Regulatory Tregs']:
    f = i_df[ip].values

    f_n = f * i_df['Leukocyte Fraction'].values
    i_df.drop(ip,axis = 1 , inplace = True)
    i_df[ip] = f_n

#Separate immune Subtypes and immune populations 

i_df['TCGA Participant Barcode'] = i_df['TCGA Participant Barcode'].str.replace('-','.',regex = False)


imm_pop = i_df[['TCGA Participant Barcode' ,
 'Leukocyte Fraction' , 'SNV Neoantigens' ,'Indel Neoantigens','B Cells Memory' , 'B Cells Naive' , 'Dendritic Cells Activated' 
, 'Dendritic Cells Resting' , 'Eosinophils' , 'Macrophages M0' , 'Macrophages M1' , 'Macrophages M2' 
, 'Mast Cells Activated' , 'Mast Cells Resting' , 'Monocytes' , 'Neutrophils' , 'NK Cells Activated' 
, 'NK Cells Resting' , 'Plasma Cells' , 'T Cells CD4 Memory Activated' , 'T Cells CD4 Memory Resting' 
, 'T Cells CD4 Naive' , 'T Cells CD8' , 'T Cells Follicular Helper' , 'T Cells gamma delta' , 
'T Cells Regulatory Tregs']]




i_df = i_df[['TCGA Participant Barcode' ,'Immune Subtype']]

# Transform msi number into MSI and MSS samples

msi = pd.read_csv(snakemake.input[1], sep ='\t')
msi = msi[['Case ID','MANTIS Score']]
msi['Case ID'] = msi['Case ID'].str.replace('-','.',regex = False)

msi['MANTIS Score'] = msi['MANTIS Score'].apply(lambda x :'MSI' if x >= 0.5 else 'MSS') 

# Merge both data

i_df = pd.merge(right = msi , left = i_df , right_on = 'Case ID', left_on = 'TCGA Participant Barcode' , copy = False, how = 'outer')


i_df['TCGA Participant Barcode'].fillna(i_df['Case ID'], inplace = True)

i_df = i_df.rename(columns = {'MANTIS Score' : 'Instability', 'Immune Subtype' : 'Immune Subtype'})
i_df = i_df[['TCGA Participant Barcode','Instability','Immune Subtype']]

i_df.to_csv(snakemake.output[0] , sep = '\t', index = False)
imm_pop.to_csv(snakemake.output[1] , sep = '\t', index = False)