import pandas as pd
import re
maf_file = snakemake.input[0]
out_file = snakemake.output[0]

def load_dataframe(input_file):
    """
    Loads input TCGA mutect .maf file as pandas dataframe, appends a project 
    column and returns it.
    """
    # Do not worry about Strand. It is always +. 
    # Source: https://www.biostars.org/p/116333/

    # Added start and end positions for maximum res. level. They won't be of use
    # but to keep duplicates at bay.

    columns_of_interest = ["Hugo_Symbol", "Chromosome","Start_Position",
    "End_Position", "Variant_Classification","Tumor_Sample_Barcode","t_depth",
    "n_depth","t_ref_count", "t_alt_count","case_id",
    "Consequence"]

    raw_maf = pd.read_csv(input_file, sep = "\t", header = 5 , 
    usecols = columns_of_interest , low_memory = False )

    return raw_maf

def preprocessing_variants(mutationData):
    """
    Filters variants based on alteration consequence and sequencing depth.
    Returns a dataframe with coding only alterations with sufficient ( > 30) 
    depth.
    """
    not_relevant = [ "SS", "RNA", 'Intron', "IGR", "Silent"]

    mutationData = mutationData[~mutationData['Variant_Classification'].isin(not_relevant)]

    # drop remaining non-exonic mutations
    # look https://github.com/mskcc/vcf2maf/issues/88 for more info about this
    # step
    is_intronic  = mutationData['Consequence'] == 'intron_variant'
    mutationData = mutationData[~is_intronic]

    # drop low depth mutations
    mutationData = mutationData[mutationData['t_depth'] >= 30]
    mutationData = mutationData[mutationData['n_depth'] >= 30]

    # Calculate variant allele frequency for each variant
    mutationData['VAF'] = mutationData['t_alt_count'] / mutationData['t_depth']

    # Drop entries with VAF < 0.1.
    mutationData = mutationData[mutationData['VAF'] >= 0.1]

    # Drop unused columns

    mutationData.drop(labels = ["Chromosome","Start_Position",
    "End_Position", "Variant_Classification","t_depth",
    "n_depth","t_ref_count", "t_alt_count","case_id",
    "Consequence"], axis = 'columns', inplace = True)

    # Generate matrix with 1 and 0
    mutationData =  pd.crosstab(mutationData['Hugo_Symbol'], mutationData['Tumor_Sample_Barcode'] , dropna = False)

    return mutationData

def filter_percentage(mutationData,per):
    filt  = mutationData.apply(sum , axis = 1)
    filt = filt/mutationData.shape[1]
    filt = filt[filt > per]
    mutationData = mutationData[mutationData.index.isin(filt.index)]
    return(mutationData)


mutations = load_dataframe(maf_file)
mutations = preprocessing_variants(mutations)
mutations[mutations > 1] = 1
# Eliminate non tumoral samples and eliminate type of sample barcode
mutations = mutations.filter(regex = '-01[A-Z]-' , axis = 1)
mutations = mutations.rename( columns = lambda x : re.sub('-01[A-Z]-.*', '', x))
mutations = mutations.loc[:,~mutations.columns.duplicated()]
#We select genes taht are mutated in a certain percentage
mutations = filter_percentage(mutations,0.00)


mutations.to_csv(out_file, sep = '\t')