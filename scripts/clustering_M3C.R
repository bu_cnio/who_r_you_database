

library(M3C)

system(' export LANGUAGE=')
system(' export LANG=en_US.UTF-8')
system('export LC_ALL=en_US.UTF-8')

data <- read.csv(snakemake@input[[1]][[1]]
                   , sep = '\t'
                   ,row.names = 1
                   ,header = T)


base <- read.table(snakemake@input[[2]][[1]]
                   , sep = '\t'
                   ,row.names = 1
                   ,header = T)


data <- log(data +1)


res <- M3C(data, cores=30, seed = 123, removeplots = T)

df <- data.frame('Names' = colnames(data2) , 'Cluster' = paste('Cluster',res$assignments, sep = ' '))


dict <- c('A','B','C','D','E','F','G','H'
          ,'I','J','K','L','M','N'
          ,'O','P','Q','R','S'
          ,'T','U','V','W','X','Y','Z')




ressub <- lapply(unique(df$Cluster), function(x){
  
  dfc <- df[df$Cluster == x,]
  dtc <- data[,dfc$Names]
  resc <- M3C(dtc, cores=30, seed = 123, removeplots = T)
  clust <- resc$assignments
  clust <- dict[clust]
  dfsb <- data.frame('Names' = colnames(dtc) , 'Sublabel' = paste('Sublabel',clust, sep = ' '))
  return(dfsb)
}
  )

dfsub <- do.call(rbind,ressub)

df <- merge(df,dfsub, by = 'Names')

base <- merge(base,dfsub, by.x = 'row.names', by.y = 'Names')

write.table(base
            , file = snakemake@output[[1]][1]
            , sep = '\t'
            ,row.names = F
            ,col.names = T)
