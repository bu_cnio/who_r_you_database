import pandas as pd
import re

maf_file = snakemake.input[0]
out_file = snakemake.output[0]
cgi = pd.read_csv(snakemake.input[1], sep = '\t', header = 0)


def load_dataframe(input_file):
    """
    Loads input TCGA mutect .maf file as pandas dataframe, appends a project 
    column and returns it.
    """
    # Do not worry about Strand. It is always +. 
    # Source: https://www.biostars.org/p/116333/

    # Added start and end positions for maximum res. level. They won't be of use
    # but to keep duplicates at bay.

    columns_of_interest = ["Hugo_Symbol", "Tumor_Sample_Barcode","t_depth",
    "n_depth","t_ref_count", "t_alt_count","HGVSp_Short"]

    raw_maf = pd.read_csv(input_file, sep = "\t", header = 5 , 
    usecols = columns_of_interest , low_memory = False )

    return raw_maf

def preprocessing_variants(mutationData ,cgi):
    """
    Filters variants based on alteration consequence and sequencing depth.
    Returns a dataframe with coding only alterations with sufficient ( > 30) 
    depth.
    """
    # drop mutations that dosen't appear in cancer genome interpreter
    mutationData['mutation'] = mutationData['Hugo_Symbol'] + mutationData['HGVSp_Short']
    cgi['mutation'] = cgi['gene'] + cgi['protein']


    mutationData = mutationData[mutationData['mutation'].isin(cgi['mutation'])]

    # drop low depth mutations
    mutationData = mutationData[mutationData['t_depth'] >= 30]
    mutationData = mutationData[mutationData['n_depth'] >= 30]

    # Calculate variant allele frequency for each variant
    mutationData['VAF'] = mutationData['t_alt_count'] / mutationData['t_depth']

    # Drop entries with VAF < 0.2. Vulcan requires this
    mutationData = mutationData[mutationData['VAF'] >= 0.1]

    # Drop unused columns

    mutationData.drop(labels = ["t_depth","n_depth","t_ref_count", 
    "t_alt_count","HGVSp_Short"], axis = 'columns', inplace = True)

    # Generate matrix with 1 and 0
    mutationData =  pd.crosstab(mutationData['Hugo_Symbol'], mutationData['Tumor_Sample_Barcode'] , dropna = False)

    return mutationData

mutations = load_dataframe(maf_file)
mutations = preprocessing_variants(mutations, cgi)
mutations[mutations > 1] = 1
# Eliminate non tumoral samples and eliminate type of sample barcode
mutations = mutations.filter(regex = '-01[A-Z]-' , axis = 1)
mutations = mutations.rename( columns = lambda x : re.sub('-01[A-Z]-.*', '', x))
mutations = mutations.loc[:,~mutations.columns.duplicated()]


mutations.to_csv(out_file, sep = '\t')