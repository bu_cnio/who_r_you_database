##################################################################################################
# script to make ssGSEA matrix
##################################################################################
############################################################
###FUNCTIONS


#Filter_samples
#'@Exp : Expression matrix with genes as rows and patients as columns
#'@output : Expression matrix with porimary and normal data without duplicated samples with different aliquotes
Filter_samples <- function(Exp){
  #01 <- primary tisue
  #11 <- normal tiisue
  
  Exp <- Exp[,grep('.01[A-Z]', colnames(Exp))]
  # We have to eliminate duplicate samples if it would exist
  samples <- gsub('.01[A-Z]$','', colnames(Exp))
  
  dup <- as.logical(duplicated(samples))
  Exp <- Exp[,!dup]
  return(Exp)
}
  


#Var_Collapse
#'@data : Expression matrix with genes as first column (NOT AS ROWNAMES)
#'@column : Name of genes column
#'@output: Expression matrix without duplicate genes in gene column. 
#'Genes with maximum variance will be selected
Var_Collapse <- function(data , column) {
  data_num <- data[,-1]
  data_num <- apply(data_num, c(1,2), as.numeric)
  data_num <- as.data.frame(data_num)
  variance <- apply(data_num,1, var)
  i_duplicate <- unique(data[[column]][duplicated(data[[column]])])
  for (i in i_duplicate){
    indx <- which(data[[column]] == i)
    max_var_indx <- indx[which(variance[indx] == max(variance[indx]))]
    indx <- indx[!indx %in% max_var_indx[[1]]]
    data <- data[-indx,]
  }
  return(data)
}


################################################################################

input <- read.table(snakemake@input[[1]][1]
                    ,sep = '\t'
                    ,header = T)

anotation <- anotation <- read.csv('databases/RNAseq_transcripts_translation.csv'
                        ,sep = ','
                        ,header = T)

project <- snakemake@wildcards[[1]][1]
#1 Anotate matix
input$Ensembl_ID <- gsub('\\..*','',input$Ensembl_ID)
input <- merge(anotation, input, by.x = 'Ensembl_ID' , by.y = 'Ensembl_ID')
input <- input[,-1]

#2 Collapse genes using those with higher variance
input <- Var_Collapse(data = input , column = 'Hugo_Symbol')
rownames(input) <- input[,1]
input <- input[,-1]

#3 Filter samples
#We filter samples using only those samples that belong from primary tumor
input <- Filter_samples(input)
# 4 we make ssGSEA
library(GSEABase)
library(GSVA)
genset <- getGmt('databases/h.all.v7.0.symbols.gmt')
ssGSEA <- gsva(as.matrix(input), genset, min.sz=5, max.sz=500, kcdf="Gaussian", mx.diff=TRUE, verbose=FALSE, parallel.sz=4, method = "ssgsea")
ssGSEA <- as.data.frame(ssGSEA)
ssGSEA <- ssGSEA[,grep('01[A-Z]',colnames(ssGSEA))]
colnames(ssGSEA) <- gsub('\\.01[A-Z]','',colnames(ssGSEA))
ssGSEA <- ssGSEA[,unique(colnames(ssGSEA))]

ssGSEA$Pathway <- rownames(ssGSEA)

# We eliminate normal samples and put names without type of sample code


ssGSEA <- ssGSEA[,order(colnames(ssGSEA))]

#5 we write results
write.table(ssGSEA 
            ,snakemake@output[[1]][1]
            ,sep = '\t'
            , col.names = TRUE
            , row.names = FALSE
            , quote = FALSE)